package edu.GMPLL.videopreview;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.VideoView;

public class MainActivity extends Activity {

	VideoView video;
	TextView Question;
	RadioGroup choiceGroup;
	Button next;
	String[] question;
	String[][] choices;
	int[] answerchoices;
	int index = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MediaController mediaController = new MediaController(this);
		int mPos = 0;
		answerchoices = new int[3];
		question = new String[] {
				"Pause the video, what should be the final answer",
				"How do you subtract a larger number from a smaller number?",
				"What answer did the tutor get?" };
		choices = new String[][] {
				{ "54", "137", "200" },
				{ "Borrow from the next tenth place if there is any",
						"Panic violently", "Switch the number" },
				{ "54", "137", "200" } };
		if (savedInstanceState != null)
			if (savedInstanceState.containsKey("pos"))
				mPos = savedInstanceState.getInt("pos");
		video = (VideoView) findViewById(R.id.Video);
		video.setMediaController(mediaController);
		Uri uri = Uri.parse("android.resource://edu.GMPLL.videopreview/"
				+ R.raw.shortmin);
		video.setVideoURI(uri);
		video.seekTo(mPos);
		video.start();
		Question = (TextView) findViewById(R.id.Question);
		choiceGroup = (RadioGroup) findViewById(R.id.ChoiceGroup);
		next = (Button) findViewById(R.id.next);
		setUpQuestion();

	}

	private void setUpQuestion() {
		if (index != 2)
			next.setText("Forward");
		else
			next.setText("Finish");
		choiceGroup.clearCheck();
		Question.setText(question[index]);
		for (int i = 0; i < choiceGroup.getChildCount(); i++)
			((RadioButton) choiceGroup.getChildAt(i))
					.setText(choices[index][i]);
	}

	public void next(View view) {
		if (index != 2)
			switch (choiceGroup.getCheckedRadioButtonId()) {
			case R.id.Choice1:
				answerchoices[index] = 1;
				index++;
				setUpQuestion();
				break;
			case R.id.Choice2:
				answerchoices[index] = 2;
				index++;
				setUpQuestion();
				break;
			case R.id.Choice3:
				answerchoices[index] = 3;
				index++;
				setUpQuestion();
				break;
			default:
				next.setText("Pick a choice");
				break;
			}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("pos", video.getCurrentPosition()); // save it here
	}

}
