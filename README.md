As mobile devices have transitioned from single purpose 
communication devices to multipurpose tools for knowledge 
sharing and generation, their users have shifted downward in age 
from adults to children. Along with this shift in computing 
technology, the nature of interaction with devices has also 
changed. Today�s mobile device users frequently switch between 
formal and informal contexts and social and individual activities 
[16] because of applications that compete for their attention 
interrupt their ongoing tasks [25]. For example when engaged in a
mobile learning activity a user might switch from the current task 
to send or respond to a text message and then return to their 
primary task [5, 36, 40]. In previous studies such off-task behavior
has been difficult to measure and considered to negatively 
impacting learning [3, 24, 37]. However, given the vast array of 
information sources and applications available via mobile devices 
a more accurate portrayal of this seemingly off-task behavior may
be that the user may be seeking additional information about 
problem solving and thus the noted behavior would be incorrectly 
labeled as off-task. As mobile learning environments grow in 
complexity with seamlessly integrated features such as video, 
Internet browsing, and email, understanding users behavior has 
become critical for making robust learning environments resulting
in the need for research in how users interact with new 
technologies. As users exhibit data-gathering actions their 
information seeking (inquiry) patterns can be used to model their 
behaviors thus enabling the learning application to more 
appropriately respond to their actions. I propose to introduce a 
new class of mobile learning