package com.GMPLL.getprocess;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.os.IBinder;
import android.text.format.Time;
import android.util.Log;

public class MyService extends Service  {
	Run thread;
	File path = Environment
            .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
	Calendar Now = Calendar.getInstance();
	File file;
	ApplicationInfo ai;
	PackageManager pm;
	@Override
	public void onCreate() {
		 pm = getPackageManager();
		thread = new Run();
		  file = new File(path, "Processes"+Now.get(Calendar.DAY_OF_YEAR) + Now.get(Calendar.HOUR_OF_DAY) + Now.get(Calendar.MINUTE) + Now.get(Calendar.SECOND) + ".txt");

	}
	
	public class Run extends Thread{
		private boolean Loop = true;
		@Override
		public void run() {
			path.mkdirs();
			while(Loop){
				

                ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
                List <RunningTaskInfo> runningTask = manager.getRunningTasks(1);
                String Times;
                Time today = new Time(Time.getCurrentTimezone());
                today.setToNow();
                Times = today.format("%k:%M:%S");
                Map<String, String> PList = new HashMap<String, String>();
                StringBuilder b = new StringBuilder();
                for(RunningTaskInfo i : runningTask){
                	try {
                	    ai = pm.getApplicationInfo(i.topActivity.getPackageName() , 0);
                	} catch ( NameNotFoundException e) {
                	    ai = null;
                	}
                	 String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
                	 if(!applicationName.equals("GetProcess") && !applicationName.equals("Launcher") && !applicationName.equals("Android System"))
                	 PList.put(applicationName, Times);
                }
                for (Map.Entry<String, String> entry : PList
                                .entrySet()) {
                        b.append(entry.getKey() + ";"
                                        + entry.getValue());
                        b.append("\n");
                }

                try {
                        OutputStream os = new FileOutputStream(file, true);
                        //b.append(file.toString());
                        
                        os.write(b.toString().getBytes()); 
                        os.close();
                } catch (Exception e) {
                        e.printStackTrace();
                }

       
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		public void setLoop(boolean loop) {
			Loop = loop;
		}

		
		
		
	}



	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	 @Override
	  public void onDestroy() {
		 
		 Log.i("GMPLL", "Whyyyyyyy :(");
		 thread.setLoop(false);
		 
	 }
	 
	 public int onStartCommand(Intent intent, int flags, int startId) {
		 if(!thread.isAlive()) {
		 thread.start();
		 }
	      return START_REDELIVER_INTENT;
	  }

}
