package com.GMPLL.getprocess;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	private Button startService;
	private Button stopService;
	//private TextView processesShow;
	Intent intent;
	        


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		startService = (Button) this.findViewById(R.id.StartProcess);
		stopService = (Button) this.findViewById(R.id.StopProcess);
	//	processesShow = (TextView) this.findViewById(R.id.Tprocess);
		startService.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// BprocessesShow.performClick();
				intent = new Intent (MainActivity.this, MyService.class);
				startService(intent);
			}
		});
		
		stopService.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(intent != null)
				stopService(intent);
			}
		});

	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}